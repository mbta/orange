# Python library from Charlie on the CLI
# Copyright (c) 2011, Matt Lee <mattl@gnu.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from suds.client import Client
import datetime

WSDL = 'http://developer.mbta.com/services/CommuterConnect.asmx?WSDL'

def feedback(APIKEY, firstName, lastName, emailAddress, commentText, feedbackType, topicType):
    """Sends feedback -- firstName, lastName, email, comment plus the current date and time."""

    client = Client(WSDL, retxml = True)

    # TODO: Better way to do this next bit?

    fooNow = datetime.datetime.now()
    dateNow = fooNow.strftime("%m/%d/%Y")
    timeNow = fooNow.strftime("%I:%M %p")

    print APIKEY, firstName, lastName, emailAddress, commentText, topicType, feedbackType

    results = client.service.AddCommuterCommentStage(
        APIKEY, dateNow, lastName, firstName, "", "", "", "", "", "", "", "", timeNow, commentText, emailAddress, topicType, feedbackType, "", "","")

    return results

if __name__ == '__main__':
    import sys
    print feedback(*sys.argv[1:8])
